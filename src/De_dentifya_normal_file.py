
from faker import Faker
from faker.providers import internet
import os

fake = Faker()


def get_name_file(input_file):
    return os.path.basename(input_file)

def replace_data_by_faker(input_file,output_file):
    with open(input_file, 'r') as txt_file, open(output_file, 'w+') as write_file:
        for line in txt_file:
            a = line.split('*')
            line = line.replace(a[3], fake.first_name())
            line = line.replace(a[4], fake.last_name())
            line = line.replace(a[9], fake.ean(length = 13)) + '~\n'
            write_file.write(line)

input_file = '../data/subscriber_name.txt'
output_file = '../out/' + get_name_file(input_file)     

if __name__ == "__main__":
    replace_data_by_faker(input_file,output_file)


        


        