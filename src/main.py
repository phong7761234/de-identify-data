 
# import pandas as pd 
import csv
from faker import Faker
from faker.providers import internet
import os

fake = Faker()

def get_name_file(input_file):
    return os.path.basename(input_file)


def replace_data_by_faker(input_file,output_file):
    with open(input_file,'r') as csv_file, open(output_file, 'w+') as write_file:       
        field_names = []
        csv_reader = csv.reader(csv_file, delimiter = ',')
        field_names = next(csv_reader)
        index_adddress1 = field_names.index('subscriber_address_line')
       
        writer = csv.DictWriter(write_file, fieldnames = field_names)
        writer.writeheader()  
        field_names[index_adddress1] = 'subscriber_address_line1'

        for row in csv_reader:
            data = dict(zip(field_names, row))
            data['subscriber_last_name'] = fake.last_name()
            data['subscriber_first_name'] = fake.first_name()
            data['subscriber_middle_name_or_initial'] = fake.random_element(elements=('M', 'L', 'P', 'Q',''))
            data['subscriber_name_suffix'] = fake.suffix()
            data['subscriber_city_name'] = fake.city()
            data['subscriber_primary_identifier']= fake.ean(length = 13).ljust(10)
            data['subscriber_state_code'] = fake.military_state()
            data['subscriber_postal_zone_or_zip_code'] = fake.postalcode_plus4()
            data['subscriber_birth_date'] = fake.date_of_birth(minimum_age = 41)
            data['subscriber_gender_code'] = fake.random_element(elements = ('M', 'F'))
            data['subscriber_address_line'] = fake.address().replace('\n', '')
            data['subscriber_address_line1'] = fake.address().replace('\n', '')
            writer.writerow(data)

input_file = '../data/2010BA_DEIDENT_INTHB0837I_286230_10160249.csv'
output_file = '../out/' + get_name_file(input_file)  

if __name__ == "__main__":
    replace_data_by_faker(input_file,output_file)

        